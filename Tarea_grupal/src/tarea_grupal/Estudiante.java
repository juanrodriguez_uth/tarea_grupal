
package tarea_grupal;
public class Estudiante extends Persona {
    
    public Estudiante(){
        this.setNombre("Rolando Estrada");
        this.setIdentidad("0601-1997-09273");
        this.setCelular("504 88758886");
        this.setProfesion("Ing. Producción Industrial");
        this.setEdad(22);
    }
    public void imprimir()
    {
        System.out.println("Nombre: "+this.getNombre()+"\nIdentidad: "+this.getIdentidad()+"\nCelular: "+this.getCelular()+"\nEdad: "+this.getEdad()+"\nProfesion: "+this.getProfesion());
    }
}
