
package tarea_grupal;
public class Deportista extends Persona {
    
    public Deportista(){
        this.setNombre("Carlos Rodriguez");
        this.setIdentidad("0201199300232");
        this.setCelular("+504 9535-1262");
        this.setProfesion("Futbolista");
        this.setEdad(29);
    }
    public void imprimir()
    {
        System.out.println(
                "Nombre: "+this.getNombre()+
                "\nIdentidad: "+this.getIdentidad()+
                "\nCelular: "+this.getCelular()+
                "\nEdad: "+this.getEdad()+
                "\nProfesion: "+this.getProfesion());
    }
}
