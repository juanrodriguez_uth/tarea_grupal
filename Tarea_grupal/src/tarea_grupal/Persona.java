
package tarea_grupal;
public abstract class Persona {
    private String nombre;
    private String identidad;
    private String celular;
    private int edad;
    private String profesion;

    public Persona(){
        
    }
    /*Nuestro metodo abstracto*/
    public abstract void imprimir();
    
    public Persona(String nombre, String identidad, String celular, int edad, String profesion) {
        this.nombre = nombre;
        this.identidad = identidad;
        this.celular = celular;
        this.edad = edad;
        this.profesion=profesion;
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentidad() {
        return identidad;
    }

    public void setIdentidad(String identidad) {
        this.identidad = identidad;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }
    
    
}
